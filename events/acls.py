from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests
from .models import Location

def get_photos(city, state):
    params = {
        "query": city + " " + state,
        "per_page": 1,
        "page": 1,
        "size": "medium",
    }
    headers = {"Authorization": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)
    print(content)
    photo = {"picture_url": content["photos"][0]["src"]["original"]}
    return photo

def get_weather_data(city, state):
    params = {
        "q": city + "," + state + "," + "US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    print(content)
    lat = content[0]["lat"]
    lon = content[0]["lon"]

    params2 = {
        "lat": lat,
        "lon": lon,
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY
    }

    url2 = "https://api.openweathermap.org/data/2.5/weather"
    response2 = requests.get(url2, params=params2)
    content2 = json.loads(response2.content)
    weather_description = content2["weather"][0]["description"]
    fahrenheight_temp = (content2["main"]["temp"] - 273.15) * 9/5 + 32
    return {"fahrenheight": fahrenheight_temp, "description": weather_description}